#!/bin/bash
#SBATCH --job-name=pyicon_qp
#SBATCH --time=08:00:00
#SBATCH --output=log.o-%j.out
#SBATCH --error=log.o-%j.out
#SBATCH --nodes=1
#SBATCH --exclusive
#SBATCH --partition=compute
#SBATCH --account=mh0033

cd /home/m/m211054/pyicon/tools

module list
source ./act_pyicon_py311.sh
which python

rand=$(cat /dev/urandom | tr -dc 'A-Z' | fold -w 3 | head -n 1)

path_pyicon=`(cd .. && pwd)`"/"
config_file="./config_qp_${rand}.py"
qp_driver="${path_pyicon}pyicon/quickplots/qp_driver.py"

cat > ${config_file} << %eof%
# --- path to quickplots
path_quickplots = '../all_qps/'

# --- set this to True if the simulation is still running
omit_last_file = True

# --- do ocean and/or atmosphere plots
do_atmosphere_plots = False
do_ocean_plots      = True

# --- grid information
gname     = 'r2b4_oce_r0004'
lev       = 'L40'

# --- path to interpolation files
path_grid        = '/work/mh0033/m211054/projects/icon/grids/'+gname+'/'
path_ckdtree     = path_grid+'/ckdtree/'

# --- grid files and reference data
path_pool_oce       = '/pool/data/ICON/oes/input/r0004/icon_grid_0036_R02B04_O/'
gnameu = gname.split('_')[0].upper()
fpath_tgrid         = path_pool_oce + gnameu+'_ocean-grid.nc'
#fpath_ref_data_oce  = path_pool_oce + gnameu+lev+'_initial_state.nc'
fpath_ref_data_oce  = '/pool/data/ICON/oes/input/r0004/icon_grid_0036_R02B04_O/R2B4L40_initial_state.nc'
#fpath_ref_data_oce  = '/work/bm1102/m211054/smtwave/initial/ts_oras5_icon_OceanOnly_Global_IcosSymmetric_0010km_rotatedZ37d_modified_sills_srtm30_1min_L128smt.nc'
#fpath_fx            = path_pool_oce + gnameu+lev+'_fx.nc'
#fpath_fx            = path_grid + gname+'_'+lev+'_fx.nc'
fpath_fx            = '/pool/data/ICON/oes/input/r0004/icon_grid_0036_R02B04_O/R2B4L40_fx.nc'


D_variable_container = dict(
  default  = '_P1M_3d',
  to       = '_P1M_3d',
  so       = '_P1M_3d',
  u        = '_P1M_3d',
  v        = '_P1M_3d',
  massflux = '_P1M_3d',
  moc      = '_P1M_moc',
  mon      = '_P1M_mon',
  ice      = '_P1M_2d',
  monthly  = '_P1M_2d',
  sqr      = '_P1M_sqr',
)



# --- time average information (can be overwritten by qp_driver call)
tave_ints = [
['2011-02-01', '2012-01-01'],
['2012-02-01', '2013-01-01'],
['2013-02-01', '2014-01-01'],
['2014-02-01', '2015-01-01'],
['2015-02-01', '2016-01-01'],
['2016-02-01', '2017-01-01'],
['2017-02-01', '2018-01-01'],
['2018-02-01', '2019-01-01'],
['2019-02-01', '2020-01-01'],
]

red_list = []
# uncomment this if ssh_variance is not there
#red_list += ['ssh']
red_list += ['ssh_variance']
# uncomment this to omit plots which require loading 3D mass_flux
#red_list += ['bstr', 'arctic_budgets', 'passage_transports']
# uncomment this to omit plots which require loading 3D u, v
red_list += ['arctic_budgets']
# uncomment this to omit plots which require loading 3D density
red_list += ['dens30w']
%eof%

# --- start qp_driver
startdate=`date +%Y-%m-%d\ %H:%M:%S`


#run="exp.ocean_omip_pg"
#path_data="/work/mh0033/m211054/projects/icon/zstar/new_ref_bb/experiments/${run}"

run="exp.ocean_omip_2a83b9b068c9"
path_data="/work/mh0033/m211054/projects/icon/zstar/zstar3_merge/experiments/${run}"

run="exp.ocean_omip_e340de964f"
path_data="/work/mh0033/m211054/projects/icon/zstar/master/experiments/${run}"

run="exp.ocean_omip_b4a441d1cd87c5e97e"
path_data="/work/mh0033/m211054/projects/icon/zstar/zstar3/experiments/${run}"


run="exp.hel22206_ocean_omip_41a0a0dc6_r2b4_zlev_default"
path_data="/work/mh0033/m211054/projects/icon/zstar/master_41a0a0dc6/experiments/${run}"



#python -u ${qp_driver} --batch=True ${config_file} --path_data=$path_data --run=$run --tave_int='2000-02-01,2050-01-01'
python -u ${qp_driver} --batch=True ${config_file} --path_data=$path_data --run=$run --tave_int='2050-02-01,2100-01-01'
python -u ${qp_driver} --batch=True ${config_file} --path_data=$path_data --run=$run --tave_int='2100-02-01,2150-01-01'
python -u ${qp_driver} --batch=True ${config_file} --path_data=$path_data --run=$run --tave_int='2150-02-01,2200-01-01'


enddate=`date +%Y-%m-%d\ %H:%M:%S`

rm ${config_file}

echo "--------------------------------------------------------------------------------"
echo "Started at ${startdate}"
echo "Ended at   ${enddate}"


