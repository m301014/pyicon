#---------------------------------------------------------------
# Prepare ERA5 data as reference for pyicon
# G. Boeloeni, 30/11/2022 
# V. Maurer, 07/2023: adapted to interpolate ERA5 data onto a 
# LAM domain with a rotated pole;
# In contrast to the original version in prep_era5_pyicon.py, 
# the horizontal interpolation is first done for the time period
# selected via yearStart and yearEnd;
# based on the interpolated 3D data, the time averaging is done to generate the 2D fields
# as well as the horizontal averaging to generate the time series

#
# To run this code:
#
# 1) set 'User settings' below
#
# 2) type:
# conda activate pyicon_py39
# python prep_era5_pyicon.py
#---------------------------------------------------------------

import sys, glob, os 
import numpy as np
from scipy.interpolate import NearestNDInterpolator
import xarray as xr
import matplotlib.pyplot as plt

#----------------------- User settings -------------------------

# input path (ERA5 data 1959-2021)
pathIn = '/hpc/uwork/icon-sml/pyICON/ERA5/'

# output path
pathOut = '/hpc/uwork/vmaurer/obs_data/regional/ERA5_pyicon/'

# start and end year for the time mean
yearStart = 2000
yearEnd   = 2005

# prefix
prefix = 'era5'

# time average interval
avgInt = 'monthly'

# do we need means over djf / jja?
do_djf = False
do_jja = False

# target grid
tgridname = 'europe025_rotated'
tgridarea = 'europe025_rotated_gridarea'
tgridpath = '/hpc/uwork/vmaurer/grids/rotated_latlon/' 

plev1 = [250, 300, 500, 700, 850, 1000]
          # pressure levels at which horizontal plots will 
          # be provided in pyicon
          # Note: pressure levels (plev2) for the lat-pres 
          # cross-section plots is read from the input

# define list of 2D variables
varFnameList2D = [
'surface_pressure',
'mean_sea_level_pressure',
'2m_temperature',
'2m_dewpoint_temperature',
'skin_temperature',
'sea_surface_temperature',
'10m_u_component_of_wind',
'10m_v_component_of_wind',
'10m_wind_speed',
'eastward_turbulent_surface_stress',
'northward_turbulent_surface_stress',
'eastward_gravity_wave_surface_stress',
'northward_gravity_wave_surface_stress',
'total_column_water_vapour',
'total_cloud_cover',
'total_precipitation',
#'large_scale_precipitation',
#'convective_precipitation',
#'surface_runoff',
'evaporation',
'surface_sensible_heat_flux',
'surface_latent_heat_flux',
'toa_incident_solar_radiation',
'top_net_solar_radiation',
'top_net_thermal_radiation',
'surface_net_solar_radiation',
'surface_net_thermal_radiation',
'surface_solar_radiation_downwards',
'surface_thermal_radiation_downwards'
]


# define list of 3D variables
varFnameList3D = [
'temperature',
'u_component_of_wind',
'v_component_of_wind',
'geopotential',
'relative_humidity',
'specific_humidity',
'fraction_of_cloud_cover',
'specific_cloud_ice_water_content',
'specific_cloud_liquid_water_content',
#-------------------------------------
# additional variables available in the raw 
# data but not used so far in pyicon:
#-------------------------------------
#'vertical_velocity',
#'vorticity',
#'divergence',
#'specific_rain_water_content',
#'specific_snow_water_content',
#'potential_vorticity',
#'ozone_mass_mixing_ratio'
]

#------------------------- Functions ---------------------------

def read_tgrid():
  # Read in the target grid onto which the ERA5 data will be interpolated.
  # This is necessary for a RotatedPole projection as lon and lat are
  # 2D fields ( lon(rlon,rlat), lat(rlon,rlat) ). Rlon and rlat (1D) are given
  # in rotated coordinates.
  # The grid must be identical to the one used in config_ckdtree
  
  # input file
  dataIn = f'{tgridpath}{tgridname}.nc'
  
  # load input as xarray.Dataset
  print ("dataIn = "+dataIn)
  dsIn = xr.open_dataset(dataIn)

  lon_o = dsIn.lon.values # lon(rlon,rlat)
  lat_o = dsIn.lat.values # lat(rlon,rlat)
  rlon  = dsIn.rlon.values
  rlat  = dsIn.rlat.values
  dsIn.close()
  
  return lon_o, lat_o, rlon, rlat

def prepcoords():

  # This function serves aesthetics, coordinates are sorted.
  # Additionally, ndarray necessary for the scipy.interpolate.NDInterpolator is calulated.

  print('')
  print('... preparing dimensions & cooridnates ...')

  # enable keeping attributes
  xr.set_options(keep_attrs=True)

  # get varFname as 1st element of varFnameList3D
  varFname = varFnameList3D[0]

  # input file
  dataIn = f'{pathIn}{prefix}_{varFname}_*_{avgInt}.nc'

  # load input as xarray.Dataset
  print ("dataIn = "+dataIn)
  dsIn = xr.open_mfdataset(dataIn)

  # rename dimensions & coordinates
  dsIn = dsIn.rename_dims({'level': 'plev2', 'longitude': 'lon', 'latitude': 'lat'})
  dsIn = dsIn.rename_vars({'level': 'plev2', 'longitude': 'lon', 'latitude': 'lat'})
 
  # select time period
  dsIn = dsIn.sel(time=slice(dateStart, dateEndP1))
  #dsIn = dsIn.sel(time=slice(dateStart, dateEnd))

  # generate ndarray (used by NearestNDInterpolator):
  lonIn     = dsIn.lon.values
  lonIn[lonIn > 180.] = lonIn[lonIn > 180.] - 360. # 0..360 to -180..180
  latIn     = dsIn.lat.values
  Loni,Lati = np.meshgrid(lonIn,latIn)
  Loni_1d   = Loni.flatten()
  Lati_1d   = Lati.flatten()
  lzip_i    = np.concatenate( (Loni_1d[:,np.newaxis], Lati_1d[:,np.newaxis]) , axis=1)

  # create output xarray.Dataset with coordinates
  dsOut = xr.Dataset(
     coords=dict(plev1=(['plev1'], plev1), 
                 plev2=(['plev2'], dsIn.plev2.values), 
                 lon=(['lon'], rlon), 
                 lat=(['lat'], rlat),
                 time=(['time'], dsIn.time.values),),
     attrs=dict(description='ERA5 data for pyicon'),
  )

  # write xarray.Dataset with coordinates to output file
  dsOut.to_netcdf(dataOut, encoding={'plev1': {'dtype': 'int32'},
                                     'plev2': {'dtype': 'int32'},
                                     'lon':   {'dtype': 'float32'},
                                     'lat':   {'dtype': 'float32'},
                                     'time':  {'dtype': 'int32'}}
                 )

  # close xarray.Datasets
  dsIn.close()
  dsOut.close()

  return lzip_i

def interp_to_rotgrid(data):
  print(f"data: {data.shape}")
  # interpolate onto a rotated latlon grid using interpND 
  interp = NearestNDInterpolator(lzip_i, data.flatten())
  return interp(lon_o, lat_o)


def prep2d():

  # Prepare 2D maps of time-mean fields.

  print('')
  print('... processing 2D fields ...')

  # enable keeping attributes
  xr.set_options(keep_attrs=True)

  # loop over 2D fields
  for varFname in varFnameList2D:

    print('... processing', varFname)

    # input file
    dataIn = f'{pathIn}{prefix}_{varFname}_*_{avgInt}.nc'

    # load input as xarray.Dataset
    dsIn = xr.open_mfdataset(dataIn, chunks={'time': 12})

    # rename dimensions & coordinates
    dsIn = dsIn.rename_dims({'longitude': 'lon', 'latitude': 'lat'})
    dsIn = dsIn.rename_vars({'longitude': 'lon', 'latitude': 'lat'})

    # select time period 
    dsIn = dsIn.sel(time=slice(dateStart, dateEndP1))

    # chunking, so that we fit to memory 
    # and hopefully get some speedup...
    dsIn = dsIn.chunk(chunks={'time': 12})
 
    
    # read variable name + values
    varName=list(dsIn.data_vars)

    # shift longitude by -180 degrees
    #dsIn.coords['lon'] = (dsIn.coords['lon'] + 180) % 360 - 180
    #dsIn = dsIn.sortby(dsIn.lon)

    """
    ####### method working on an individual time step ###########
    values = dsIn[varName[0]].values
    # horizontal interpolation (lzip_i specifies the target grid)
    interp = NearestNDInterpolator(lzip_i, values.flatten())
    new_values = interp(lon_o, lat_o)

    # test: plot first field
    plt.pcolormesh(lon_o, lat_o, new_values, shading='auto')
    plt.show()

    # put interpolated values into a xarray dataArray
    dsOut      = xr.DataArray(new_values,
                        coords={'lat': rlat,'lon': rlon},
                        name=varName[0],
                        attrs=dsIn[varName[0]].attrs
                        )
    """
    
    # interpolate to output resolution
    # apply_ufunc derives the loop over time dim from the arguments (core_dims, exclude_dims)
    dsOut = xr.apply_ufunc(interp_to_rotgrid,
                     dsIn.load(),
                     input_core_dims = [["lat", "lon"]],
                     output_core_dims = [['new_lat', 'new_lon']],
                     exclude_dims=set(("lat","lon")),      # dimensions allowed to change size. Must be a set!
                     vectorize=True,                       # loop over non-core dims
                     dask="parallelized"
                     )

    dsOut = dsOut.rename({"new_lat": "lat", "new_lon": "lon"})
    dsOut["lat"] = rlat
    dsOut["lon"] = rlon

    ### test: plot 1st time step
    #print(dsOut)
    #plt.pcolormesh(lon_o, lat_o, dsOut.t2m.values[0,:,:], shading='auto')
    #plt.show()

    # calculate + save time series (only if none of the seasonal subsampling is chosen)
    if not (do_djf and do_jja):
      prepgmts(dsOut)

    # optionally select djf/jja months
    if do_djf:
      dsIn = dsIn.sel(time=is_djf(dsIn['time.month']))
    if do_jja:
      dsIn = dsIn.sel(time=is_jja(dsIn['time.month']))
    
    # calculate time-mean
    dsOut = dsOut.sel(time=slice(dateStart, dateEnd)).mean(dim='time')
    #dsOut = dsOut.mean(dim='time')

    ## update variable attributes to see what was done
    var_atts = dsOut[varName[0]].attrs             # var_atts is a dict -> working on DataSet
    value1   = 'time: mean ('+str(yearStart)+'-'+str(yearEnd)+')'
    new_dict = {'cell_methods' : value1}       # create new_dict
    var_atts.update(new_dict)                  # update dict var_atts with new_dict
    dsOut[varName[0]].attrs = var_atts                     # and set the variable attribute

    # write (append) output xarray.Dataset to output file
    dsOut.to_netcdf(dataOut, mode='a')

    # close xarray.Datasets
    dsIn.close()
    dsOut.close()

  return

def prep3dmaps():

  # Prepare 3D fields on a reduced set of pressure 
  # levels (plev1), i. e. 2D maps at plev1 levels.

  print('')
  print('... processing 3D fields on pressure levels plev1 ...')
  if yearEnd-yearStart > 15:
    print('... !!! Beware: this might take a while for long periods !!! ...')

  # enable keeping attributes
  xr.set_options(keep_attrs=True)

  # loop over 3D fields
  for varFname in varFnameList3D:

    print('... processing', varFname)

    # input file
    dataIn = f'{pathIn}{prefix}_{varFname}_*_{avgInt}.nc'

    # load input as xarray.Dataset
    dsIn = xr.open_mfdataset(dataIn, chunks={'time': 12, 'level':1})

    # rename dimensions & coordinates
    dsIn = dsIn.rename_dims({'level': 'plev2', 'longitude': 'lon', 'latitude': 'lat'})
    dsIn = dsIn.rename_vars({'level': 'plev2', 'longitude': 'lon', 'latitude': 'lat'})

    # select time period 
    dsIn = dsIn.sel(time=slice(dateStart, dateEnd), plev2=plev1)

    # optionally select djf/jja months
    if do_djf:
      dsIn = dsIn.sel(time=is_djf(dsIn['time.month']))
    if do_jja:
      dsIn = dsIn.sel(time=is_jja(dsIn['time.month']))

    # rename dimensions & coordinates
    dsIn = dsIn.rename_dims({'plev2': 'plev1'})
    dsIn = dsIn.rename_vars({'plev2': 'plev1'})

    # calculate time-mean (as now time series is created for 3D vars,
    #                      we can do the time averaging first)
    dsIn = dsIn.mean(dim='time')
    
    # interpolate to output resolution
    # apply_ufunc derives from the arguments (core_dims, exclude_dims) the loop over z dim
    dsOut = xr.apply_ufunc(interp_to_rotgrid,
                     dsIn.load(),
                     input_core_dims = [["lat", "lon"]],
                     output_core_dims = [['new_lat', 'new_lon']],
                     exclude_dims=set(("lat","lon")),      # dimensions allowed to change size. Must be set!
                     vectorize=True,                       # loop over non-core dims
                     dask="parallelized"
                     )
    
    dsOut = dsOut.rename({"new_lat": "lat", "new_lon": "lon"})
    dsOut["lat"] = rlat
    dsOut["lon"] = rlon

    # edit variable attributes to indicate that they are temporally averaged
    varName=list(dsIn.data_vars)[0]
    var_atts = dsOut[varName].attrs                     # var_atts is a dict    
    value1 = 'time: mean ('+str(yearStart)+'-'+str(yearEnd)+')'
    new_dict = {'cell_methods' : value1}                # create new_dict
    var_atts.update(new_dict)                           # update dict var_atts with new_dict   
    dsOut[varName].attrs = var_atts                     # and set the variable attribute

    # reverse pressure levels
    dsOut = dsOut.reindex(plev1=list(reversed(dsOut.plev1)))

    # write (append) output xarray.Dataset to output file
    dsOut.to_netcdf(dataOut, mode='a')

    # close xarray.Datasets
    dsIn.close()
    dsOut.close()

  return


def prepgmts(dsIn):

  # Prepare global-mean time-series of 2D fields. 
  # In contrast to the original version in prep_era5_pyicon.py, this
  # is only done for the time period selected via yearStart and yearEnd
  # (see the header of this script) as the order of the data processing is changed:
  # 1) horizontal interpolation (otherwise, not the exact LAM domain is selected)
  # 2) call prepgmts

  print('')
  print('... processing global-mean time-series of 2D fields ...')

  # enable keeping attributes -> check!
  #xr.set_options(keep_attrs=True)

  # rename variable to represent global mean ts
  varName=list(dsIn.data_vars)[0]
  dsIn = dsIn.rename_vars({varName: varName+'_gmts'})

  # chunking, so that we fit to memory 
  # and hopefully get some speedup...
  dsIn = dsIn.chunk(chunks={'time': 12})

  # input of cell area, calculation of weights
  area = calc_cell_fraction()

  # create xarray.Datarray
  area_xr = xr.DataArray(area, 
            coords={'lat': dsIn.lat.values,'lon': dsIn.lon.values} 
            )
 
  # calculate cell-area weighted global-mean
  dsWg = dsIn.weighted(area_xr)
  dsOut = dsWg.mean(dim=['lon', 'lat'])

  # write (append) output xarray.Dataset to output file
  dsOut.to_netcdf(dataOut, mode='a')

  # close xarray.Datasets
  dsIn.close()
  dsOut.close()

  return

def calc_cell_fraction():

  # Read the area of each grid cell (prepared offline by cdo gridarea). 
  # This is needed for calculating a cell-area weighted global mean.
  dataIn = f'{tgridpath}{tgridarea}.nc'
  
  # load input as xarray.Dataset
  print ("dataIn = "+dataIn)
  dsIn = xr.open_dataset(dataIn)
  area = dsIn.cell_area.values[:,:]

  dataIn = f'{tgridpath}{tgridname}.nc'
  
  # area fraction w.r.t. Earth's surface area
  area = area / np.sum(area)

  return area


# functions to prepare seasonal mean fields
def is_djf(month):
  return (month == 12) | (month == 1) | (month == 2)

def is_jja(month):
  return (month == 6) | (month == 7) | (month == 8)


#-------------------------------------------------------------------
#---------------------------- Main part ----------------------------
#-------------------------------------------------------------------

if do_djf and do_jja:
  print('')
  print('do_djf and do_jja should not be True at the same time!')
  sys.exit()

if do_djf:
  print('')
  print('Preparing ERA5 data for pyicon for the period:', yearStart,'-', yearEnd, ' DJF')
elif do_jja:
  print('')
  print('Preparing ERA5 data for pyicon for the period:', yearStart,'-', yearEnd, ' JJA')
else:
  print('')
  print('Preparing ERA5 data for pyicon for the period:', yearStart,'-', yearEnd)


# dates corresponding to yearStart, yearEnd
dateStart = np.datetime64(str(yearStart)+'-01-01')
dateEnd   = np.datetime64(str(yearEnd)+'-12-01')
dateEndP1 = np.datetime64(str(yearEnd+1)+'-01-01')
print("dateEnd = ")
print(dateEnd)
print("dateEndP1 = ")
print(dateEndP1)

# read target grid
lon_o, lat_o, rlon, rlat = read_tgrid()

# define output file
if do_djf:
  dataOut = f'{pathOut}{prefix}_pyicon_{yearStart}-{yearEnd}_djf_{tgridname}.nc'
elif do_jja:
  dataOut = f'{pathOut}{prefix}_pyicon_{yearStart}-{yearEnd}_jja_{tgridname}.nc'
else:
  dataOut = f'{pathOut}{prefix}_pyicon_{yearStart}-{yearEnd}_{tgridname}.nc'

# prepare dims & coords
lzip_i = prepcoords()

# prepare 2D fields
prep2d()

# prepare 3D maps
prep3dmaps()


print('')
print('All done!')
print('Output file: ', dataOut)

#---------------------------------------------------------------
