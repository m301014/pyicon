#!/usr/bin/env python
import sys
import glob

flist = glob.glob('../../notebooks/*.ipynb')
flist.sort()

redlist = ['examp_pyicon_start_Mac', 'test_pyicon_section']

for fpath in flist:
  name = fpath.split('/')[-1].split(".ipynb")[0]
  fpath_link = './' + name + '.nblink' 
  if name in redlist:
    print(f'Ignorring: {name}')
  else:
    print(f'Linking: {fpath} -> {fpath_link}')
    with open(fpath_link, 'w') as f:
      f.write(f'{{\n    "path": "{fpath}"\n}}')

