#!/bin/bash

echo "************************************************************" 
echo " 01_download_example_data.sh"
echo "************************************************************" 

source_dir=`cat source_dir.txt`

cwd=`pwd`
mkdir -p ${source_dir}
cd ${source_dir}
wget -r -H -N --cut-dirs=3 --include-directories="/v1/" "https://swiftbrowser.dkrz.de/public/dkrz_07387162e5cd4c81b1376bd7c648bb60/pyicon_example_data/?show_all"
cd ${cwd}
